# Java Modules

## Non-Modular Java characteristics

-  Packages provided logical grouping of classes
- Packaes did not impose physical restrictions on how they are used
- Classes are packaged into jar file and accessed via classpath
- Common deployment of related classes is not enforced
- Visibility of classes is controlled with public and default access modifiers
- Encapsulation can always be bypassed using reflection
- Impossible to restrict in with exact other packages your code can be used.

Note: *Reflection allows access to object fields and methods, regardless of their access modifiers.* This really means that no Java code is, strictly speaking, really encapsulated.

## What Is a Module

Module is high-level code aggregation.
* It comprises one or more closely related packages and other resources such as images or xml files.
* Module descriptor `module-info.class` stored in the module's root folder contains:
	- Unique module name (recommanded reverse-dns convention `org.enable.analyzer`)
	- Required module dependencies (other modules that this module depends on)
	- Packages that this module exports, making them available to other modules (all other packages contained within the module are unvailable to other modules).
	- Permissions to open content of this module to other modules using reflection.
	- Services this module offers to other modules
	- Services this module consumes
	- Modules do not allow splitting Java packages even when are not exported (private)
	
### Module Template

```java
	module <this modules name> {
		requires <other module names>;
		exports <packages of this module to other modules that require them>;
		opens <packages of this module to other modules via reflection>;
		uses <services provided by other modules>;
		provides <services to other modules with services implementations>;
	}

```
## Java Platform Module System (JPMS)

Module is a group of packages.

Module declares which other modules it uses:
* Enables smaller application deployment footprint
- Circular module dependencies are not allowed
- Dependencies between modules can be verified at application startup

Classes of the modularized applications are loaded using module-path, not class-path
- Deployment errors, such as missing modules, are detected at startup.

### JPMS Module Categories

- Java SE Modules
	* Core Java platform for general-purpose APIs
	* Module names start with "java"
	* Exple: `java.base`, `java.se`
- JDK Modules
	* Additional implementation-specific modules
	* Module names start with "jdk"
	* Exple: `jdk.jconsole`, `jdk.jshell`
- Other Modules
	* Create your own and use third-party modules.
	
### Define Module Dependencies
Module defines which other modules it needs
* **requires** <modules> directive specifies a normale module dependency
* **requires transitive** module directive makes dependent <modules> available to other modules.
* **requires static <modules> directive indicates module dependency at compile time only.

```java
	module com.some {
		requires java.logging;
		requires transitive org.enable
		requires static com.upowa
	}
```

*Notes:*
* These instructions accept comma-separated lists of module names
* Directive `requires java.base` is implied for all modules, but any other module has to be referenced explicitly.

### Export Module Content
* Exporting a package means making all of its public types (and their nested public and protected types) available to other modules.
* `exports` <packages> directive specifies packages whose public types should be accessible to all other modules.
* `exports` <packages> to <other modules> restricts exported packages to a list of specific modules.


### Open Module Content

Module may allow runtime-only access to a package using `opens` directive.
* `opens` packages directive specifies package whose entire content is accessible to all other modules at run time.
* `opens` packages to modules restricts opened package to a list of specific modules.
* Opening a package works similar to export, but also makes all of its non-public types available via reflection.
* Modules that contain injectable code should use `opens` directive, because *injections work via reflection*.

### Open an Entire Module
Module may allow runtime-only access to all of its content.
* `open module` specifies that this module's entire content is accessible to all other modules at run time via reflection.

```java
	open module demos {}
```
### Produce and Consume Services

Modules can produce and consume services, rather than just expose all public classes in selected packages.
* Service comprises an interface or abstract class and one or more implementation classes
* `provides` <service interface> `with` <classes> directive specifies that module provides one or more service implementations than can be **dynamically discovered by service consumer**.
* `uses` <service interface> directive specifies an interface or an abstract class that defines a service that this module would like to consume.

```java
	module provider {
		requires service;
		provides service.a.L with provider.b.M;
	}
	
	public class Main {
		public static void main(String[] args){
			ServiceLoader<L> sl = ServiceLoader.load(L.class);
			L l = sl.findFirst().get();
		}
	}
	
	module application {
		requires service;
		uses service.a.L;
	}
```
`provides service with <implementation classes>`


## Compile and Package a Module

Compile Module:
* Specify all of your java sources from various packages that you want this module to contain
* Include packages that are exported by this module to other modules and a module-info
* Reference other modules required for this module to compile

- .jav to .class
```java
	javac --module-path <path to other modules> -d <compiled output folder> -sourcepath <path to source code>
```

- Package module into q JAR file (.class to .jar)
* You may describe a main class for this module, if appropriate
* "." indicates inclusion of all files from a compiled code folder.

```java
	jar --create -f <path and name of the jar file> --main-class <package name>.<main class name> -c <path to compiled module code>
	
```

- Verify Packaged module

Get description of the compiled module to find out which modules it contains, exports, requires, etc.

```java
	java --module-path <path to compiled module> --describe-module <module-name>
```

`--create` option instructs jar utility to create new jar file.

`-f` option sets path to the car file.

`-C` option sets path to compiled code of the module.😎️

If module has not explicitly defined that it required java.base and relied upon an implicit inclusion, then `--describe-module` command would display this as `requires java.base mandated.`

Existing modules can be update using `--update` option.

### Execute a Modularized Application

Execute Modular application Using Module Path
* Classes are located using `-p` or `--module-path` option
* Reference the main class using `-m` or `--module` option
* Non-modular JARs are treated as automatic modules.

```java
	java -p <path to module> -m <module name>/<package name>.<main class name> <arguments>
```

Execute Non-modular Application
* Classes are located using `-cp` or `class-path` option
* Modular JARs located via class path are treated as non-modular for backward compatibility

```java
	java -cp <path to jars including modularised jars> <package name>.<main class name> <arguments>	
```


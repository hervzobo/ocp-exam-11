# Course goals
In this course, you learn how to implement application logic using Java SE:
 - Describe the object-oriented programming approach;
 - Explain Java syntax and coding conventions;
 - Use Java construct and operators;
 - Use core Java API, such as collections, Streams, IO, and concurrency;
 - Deploy Java SE applications

## What is Java?
 - General purpose programming language similar to C and C++;
 - Object-oriented and platform-Independent
 - Originally designed in 1995 for use in consumer electronics;
 - Modern uses include writing applications for internet of things, cloud computing, and so on;
 
### Java Editions:
 * Java Card - Smart Card Edition
 * Java ME - Micro Edition
 * Java SE - Standard Edition
 * Java MP - Micro Profile
 * Java EE - Enterprise Edition

## How Java works?
Java is a platform-independent programming language.
 * Java source code is written an plain text .java files
 * Source code is compiled into byte-code .class files for JVM
 * Java Virtual Machine must be installed on a target computer;
 * JVM executes your application by translating java byte-code instructions to platform-specific code

*Note*: A java program has to be compiled only once to work on any platform

## Classes
Class and Object are two key object-oriented concepts.
Java code is structured with classes.
* Class represents a type of thing or a concept, such as Dog, Cat, Person, Ball.
* Each class defines what kind of information (attributes) it can store:
	- A Dog could have a name, color, size.
	- A Ball would have type, material, and so on.
* Each class defines what kind of behaviors (operations) contenaing program logic (algorithms) it is capable of:
	- A Dog could bark and fetch a Ball.
	- A Cat could meow but is not likelyto play fetch.

``
	class Person {
		void play() {
			Dog dog = new Dog();
			dog.name = "Rex";
			Ball ball = new Ball();
			dog.fetch(ball);
		}
	}
``

``
	class Dog {
		String name;
		fetch(Ball ball){
			ball.find();
			ball.chew();
		}
	}
``
## Objects
An Object is a specific *instance* (example of) a class.
* Each object would be capable of having *specific values* for each attribute defined by a class that represents its type. For example:
	- A dog could be called Fido, and be brown and small.
	- Another could be called Rex, and be orange and big.
* To operate on an object, you can **reference** it using a variable of a relevant type.
* Each object would be capable of behaviors defined by a class that represents its type:
	- At run time, objects **invoke operations** upon each other to execut program logic.
	
## Inheritance
You can reuse (inherit) attributes and behaviors across class hierarchy.
* Classes can form hierarchical relationships.
* Superclass represents a more generic, parent type (living organism).
* Superclasses define common attributes and behaviors (eat, propagate).
* A subclass represents a more specific, child type (animal, plant, and so on).
* Thre could be any number of levels in the hierarchy, from very generic to very specific child types (dog, cat, and so on).
* Subclasses inherit all attributes and behaviors from their parents.
* Subclasses can define more specific attributes and behaviors (swim, fly).

## Java APIs
Java Development Kits (JDK) provides hundreds of classes for various programming purposes:
* To represent basic data types, for example: *String, LocalDateTime, BigDecimal*, and so on.
* To manipulate collections, for example: *Enumeration, ArrayList, HashMap*, and so on
* To handle generic behaviors and platform system actions, for example, *System, Object, Class*, and so on.
* To perform Input/Ouput (I/O) operation, for example, *FileInputStream, FileOuputStream*, and so on.
* Many other API classes are used to access databases, manage concurrency, enable network communications, execute scripts, manage transactions, security, logging, build graphical user interfaces, and so on.

*Notes*:
- Application Programming Intefaces (API) is a term that describes a collection of classes that are designed to serve a common purpose.
- All Java APIs are thoroughly documented for each version of the language.

## Java Naming Conventions
* Java is case-sensitive; Dog is not the same as dog.
* Package name is a reverse of your company domain name, plus the naming system adopted within your company.
* Class name should be a noun, in mixed case starting with a lowercase letter; further words start with capital letters.
* Name should not start with numeric characters (0-9), underscore _ or dollar $ symbols.
* Constant name is typically written in uppercase with underscore symbols between words.
* Method name should be a verb, in mixed case starting with a lowercase letter; further words start with capital letters.

*Note:* The use of the _ symbol as a first or only character in a variable name produces a compiler warning in Java 8 and an error in Java 9 onward. 

# Nested Classes and Lambda Expressions

## Types of Nested Classes

Classes can be defined inside other classes to encapsulate logic and constrain context of use =.
* Type of the nested class depends on the context in which it is used.
	- Static nested class is associated with the static context of the outer class.
	- Member inner class is associated with the instance context of the outer class.
	- Local inner class is associated with the context of specific method.
	- Anonymous inner class is an inline implementation or extension of an interface or a class.
* Static and member nested classes can be defined as:
	- *public, protected, or default* can be accessed externally
	- *private can be referenced only inside their outer class.*
```java 
	public class Outer {
		public static class StaticNested {
			//code of the nested class
		}
	}
	Outer.StaticNested x = new Outer.StaticNested();
```

## Nested Classes' Design Considerations

Serialization of any nested classes is possible, but not recommended, as a probability of the serialized object across different implementations of Java runtimes is not guaranteed.


## Static Nested Classes

Static nested class is associated with the **static context of the outer class**.
* To create an **instance of a static nested class**, you do not need to create *instances of outer class*.
* Can access private variables and methodes of the outer class.
* Can only access static variables and methods of the outer class.

## Member Inner Classes

Member inner class is associated with the instance context of the outer class
* To create an *instance of a member class*, you must create *an instance of the outer class* first.
* Can access private variables and methods of the outer class.
* Can access both static and instance variables and methods of the outer class (class externe).

## Local Inner classes

Local inner class is associated with the context of specific method.
* *Instances of the inner class* can only be created within the **outer method context**
* Contains logic complex enough to require the algorithms be wrapped up as a class.
* Outer method local variables and parameters can only be accessed if they are **final or effectively final**.

## Anonymous Inner Classes

Anonymous inner class is an implementation of an interface or extension of a class.
* **Extend a parent class or implement an interface** to override operations.
* Implemented inline and instantiated immediately.
* Outer method local variables and parameters can only be accessed if they are final or effectively final.

## Anonymous Inner classes and Functional Interfaces

Anonymous inner classess are typically used to provide inline interface implementations.
* Anonymous inner class can implement an interface inline and override as many methods as required.
* **Functional interfaces** define only one abstract method that must be overridden.
* Anonymous inner class that implements functional interface will only have to override one method.
* It could be more convenient to:
	- Use a regular class to override many methods
	- Use anonymous inner class to override a few methods (just one in case of a functional interface)
	
## Understand Lambda Expressions

Lambda expression is an inline implementation of a functional interface.
* Lambda expression infers its properties from the context:
	- **Context of invocation infers which functional interface is implemented.**
	- **Functional interface's only abstract method infers the method that has to be overridden**.
	- **Generics infer which parameters the method should have.**
	- **Return type infers a return statement**
* Lambda expression may just certain parameter name and the desired algorithm.
* Lambda token **->** separates parameter list from the method bod.

**Invocation context**:

```java 
	List<Product> products = ...
	 Collections.sort(products, <Comparator>);
```

**Anonymous inner class**:

```java
	Collections.sort(products, new Comparator<Product>() {
		public int compare(Product p1, Product p2) {
			return p1.getName().compareTo(p2.getName());
		}
	});
```

**Lambda expression**:

`Colections.sort(products, (p1, p2) -> p1.getPrice().compareTo(p2.getPrice()));`


## Define Lambda Expression Parameters and Body

Parameter definitions of lambda expressions:
* To **apply modifiers** (annotations or keywords) to parameters, define them using:
	- Specific types
	- Locally inferred types
* When no modifier is required, you may just **infer types from the context.**
* Formal body `{}` and `return` statements are optional when usinf a simple expression.
* Round brackets for parameters are also optional.
* Expressions can be **predefined and reused**

## Use Method References

Lambda expressions may use method referencing.
* *Reference method* is semantically identical to the method that lambda expression is implementing.
* `<class>::<staticMethod>` reference a static method
* `<object>::<instanceMethod>` reference an instance method of a particular object
* `<class>::<instanceMethod>` reference an instance method of an arbitrary object of a particular type
* `<class>::new` reference a constructor.

### Use Default and static Methods of the **Comparator** Interface

Examples of default methods provided by the `java.util.Comparator` interface:
* `thenComparing` adds additional *comparators*
* `reversed` reverse sorting order

Exampls of static methods provided by the comporator interface:
* `nullsFirst` and `nullsLast` return *comparators* that enable sorting collections with null values.

### Use Default and Static Methods of the `Predicate` Interface

Default methods provided by the `java.util.function.Predicate` interface:
* `and` combines *predicates* like the `&&` operator
* `or` combines *predicates* like the `||` operator
* `negate` returns a *predicates* that represents the logicial negation of this predicate

Static methods provided by the Predicate interface:
* `not` returns a *predicate* that is the negation of the supplied *predicate*
* `isEqual` returns a *predicate* that compares the supplied object with the contents of the collection.

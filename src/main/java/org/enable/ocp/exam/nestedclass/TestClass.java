/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam.nestedclass;

/**
 *
 * @author yvon
 */
public class TestClass {

    private class T {

    }

    public class A {

    }

    public static class B {

    }

    public static void main(String[] args) {
        class C {

            TestClass.A a = new TestClass().new A();
        }
        //TestClass.B b = new TestClass().new B(); //doesn't compile
        TestClass.A a = new TestClass().new A(); //compile
        //TestClass.B b1 = new TestClass().B(); //doesn't Compile, B isn't instance method
        TestClass.B b2 = new TestClass.B();
        TestClass t = new TestClass();
        TestClass.A A1 = t.new A();

    }
}

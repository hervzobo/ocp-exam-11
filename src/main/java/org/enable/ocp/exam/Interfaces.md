# Java Interfaces

An interface defines a set of features that can be applied to various other classes.
* Instance methods are by default *public and abstract*.
* They can contain concrete methods **only** if they are either *default* or *private* or *static*.
* They can contain **constants**, but not variables.

*note*: Interface resembles an abstract class, except no variables or regular concrete methods are allowed.

The default method is a special type of operation that allows provision of concrete implementation code within the interface; otherwise, public instance methods of an interface must all be abstract.

## Multiple Inheritance Problem

Object-oriented inheritance is not very flexible.
* Depending on  the point of view, a given class may require different parents (to be of more than one type).
* Different sets of features can be inherited through the extension of different types.
* Java class can only extend one immediate superclass. (**Multiple inheritance is not allowed in Java.**)
* Other object-oriented languages that do allow multiple inheritance face conflicts between parent type, when two or more parents provide concrete operations with identical signatures or variables with identical names.

## Implement Interfaces

Interfaces solve multiple inheritance problem:
* Class can implement as many interfaces as needed (regular concrete methods are not allowed in interfaces).
* A concrete class must provide a *concrete method implementation* for each **abstract method signature** declared by interfaces that it implements.
* Default method can only be defined in an interface.
* A class must override **default interface method** *only if it conflicts with another default method* (implementing different interfaces that define default with the same signature).

*Reminder*: Narrowing access to the concrete method, which implements an abstract method, is not allowed.

## Default, Private, and Static Methods in Interfaces

Concrete code can be present in the interface only within the default, private, or static method.
* Private interface methods do not cause conflicts, because they are not visible outside of that interface.
* Static interface methods do not cause conflicts, because they are invoked via specific parent types and do not rely on the super reference.
* If there is a conflict between default methods, it must be resolved by overriding this default method within the implementation class.
* Otherwise, the default method implementation can be inherited.

## Interface Hierarchy

An interface can extend another interface.
* Interfaces can form hierarchy in the same way as classes.
* A concrete class must override all abstract methods that it has inherited, regardless of where they were defined.

## Interface Is a Type

An interface is a type just like a class.
* Interface is a valid reference type, can be used in type casting, and works with the `instanceof` operator.
* The reference declared as a specific class type enables access to all publicly visible capabilities of this class, including methods defined by its parent classes, and interfaces its implements.
* The reference declared as interface type enables access only to those operations that are described by this interface and its parents.
* Object class is the ultimate parent type in Java. That is why methods of the Object class are accessible via any type of reference.

## Functional Interface

Functional interface is an interface that defines a single abstract operation (function).

**Problem:**
* It is not possible to partially implement an interface. The concrete class must implement all abstract methods that it has inherited.
* An interface with many abstract methods is not convenient to use. A class may have to provide implementations for methods that it does not actually need.

**Solution:**
* A class may implement as many interfaces as needed.
* An interface can define just one abstract method; no extra operations that could be "unwanted baggage" has to be implemented.
* It is a flexible and recommended approach to designing interfaces.

*Note:* An interface is considered to be functional if it defines just one abstract method. However, yoiu can indicate that specific interface is functional using the `@FunctionInterface` annotation. 

## Generics

Generics is a feature of Java language available since java SE 5.
* It allows variables and methods to operate on objects of various types while providing compile-time type safety.
* Versions prior to Java SE5 (no generic style) wrap values within the class using the type Object.
* Post Java SE5 (generics style) versions wrap values within the class, but defer the exact type identification.
* Generic type avoids hard-coding the exact type as part of the class design.

```java
	public class Some {
	 	private Object value;
	 	public Object getValue(){
	 		return value;
	 	}
	 	public void setValue(Object value){
	 		this.value = value;
	 	} 
	 }
	 
	  class Some<T> {
	 	private T value;
	 	public T getValue(){
	 		return value;
	 	}
	 	public void setValue(T value){
	 		this.value = value;
	 	} 
	  }
```
  
  
  *Note:* In the example, T is not a keyword, or class, or interface name, but a **generic** type maker. Other markers can be used: *T (type)*, *V(value)*, *K (Key)*, and any other marker you like, which could be a word or even a single letter.
  
  Generics cannot be applied to:
  * Descendants of class Throwable
  * Anonymous classes
  * Enumerations
  

## Use Generics

The use of generics helps to produce compact, type-safe code.
* Without generics:
	-  **Any type** can be assigned to a variable or parameter whose type is Object
	- Programmatic type-check using the `instanceof` operator is required to ensure that yu don't accidentally cast variable to the wrong type.
* With generics:
	- Compiler checks that the type that is assigned, or passed as parameter corresponds to the *generic type* declaration, rejecting code that attemps to use types that don't match.
	- No progammatic type-check or type-casting is required. 

*Note:* Generics can be used with both classes and interfaces. many existing Java interfaces utilize generics.

# Collections API

## Introduction to Java Collection API

Collection API presents a number of classes that manipulate-groups of objects (Collections).
* Collection classes may provide features such as:
	- Use generics
	- Dynamically expand as appropriate
	- Provide alternative search and index capabilities
	- Validate elements within the collection (for example, check uniqueness)
	- Order elements
	- Provide thred-safe operations (protect internal storage from corruption when it is accessed concurrently from multiple threds)
	- Iterate through the collection
* Collection API defines a number of interfaces describing such capabilities
- Collection API classes implement these interfaces and can provide different combinations of capabilities

*Note:* Collection API provides much flexibility compared to just using arrays.

## Java Collection API Interfaces and Implementation Classes

Java collection API provides automation and convenience wrappers for array handling.
* Located in `java.util` package
* **Collection API Interfaces:**
	- `Iterable<T>` is a tp-level interface that allows any collection to be used in a forEach loop.
	- `Collection<E>` interface extends `Iterable` and describes generic collection capabilities, such as adding and removing elements.
	- `List<E>` interface extends `Collection` and describes a list of elements indexed by `int`.
	- `Set<E>` interface extends `Collection` and describes a set of unique elements
	- `SortedSet<E>` interface extends `Set` to add ordering ability
	- `Deque<E>` interface extends `Collection` and describes a double-ended queue, providing **First-In-First-Out (FIFO) and Last-In-First-Out (LIFO)** behaviors.
	- `Map<K, V>` interface contains a `Set` of unique keys ans a `List` of values
* **Collection API Classes:**
	- `ArrayList<E>` implements `List` interface
	- `HashSet<E>` implements `Set` interface and `TreeSet<E>` implements `SortedSet`
	- `ArrayDeque<E>` implements `Deque` interface
	- `HashMap<K, V>` implements `Map` interface.
	
*Note:* this is a frequently used collection types, more variants of collections are available.

### Create List Object

Class `java.utli.ArrayList` is an implementation of the `List` interface
* Instance of `ArrayList` can be created using:
	- No-argument constructor, creating a list of initial capacity of 10 elements
	- Constructor with specific initial capacity
	- Any other `Collection` (for exaple `Set`) to populate this list with initial values
* A fixed-sized `List` can be create using a var-org method `Arrays.asList(<T> ...)`
* A read-only instance of `List` can be created using a var-arg method `List.of(<T> ...)`
* ArrayList will auto-expand its internal storage, when more elements are added to it.

### Manage List Contents

`List` reprsents collection of elements dex by `int`
* Insert  `boolean add(T)`  `boolean add(int, T)`
* Update   `boolean set(int, T)`
* Delete   `boolean remove(int)` ; `boolean remove(T)`
* Check Existence  `boolean contains(T)`
* Find an index  `int indexOf(T)`

### Create Set Object & Manage contensts

#### Create Set Object

Class `java.util.HashSet` is an implementation of the `Set` interface.
* Instance of `HashSet` can be create using:
	- No-arg constructor, creating a ste of initial capacity of 16 elements
	- Constructor with specific initial capacity
	- Constructor with specific initial capacity and load factor (default is 0.75)
	- Any other `Collection` (for example, `List`) to populate this set with initial values
* HashSet will auto-expand its internal storage when more elements are added to it.
* A read-only instance `Set` can be created using var-arg method `Set.of(<T> ...)`

```java
	Set<Product> ps = new HashSet<>();
	Set<Product> ps1 = new HashSet<>(20);
	Set<Product> ps2 = new HashSet<>(20, 0.85);
```

*Notes:*
* The load factor is a measure of how full the hash table is allowed to get before its capacity is automatically increased.
* List allows duplicate elements while Set does not; When Set is populated based on List duplicate entries are discarded.
* Reminder: var-arg parameter accepts a number of parameters or an array.

#### Manage Set Contents
`Set` represents collection of unique elements
* Insert  `boolean add(T)`
* Delete `boolean remove(T)`
* Check Existence  `boolean contains(T)`
* Dupplicate element cannot be added to the set:
	- Methods `add` and `remove` verify if the element exists in the set using `equals` method
	- Methods `add` and `remove` will return `false` value when attempting to add a duplicate or remove an absent element.
	
### Create Deque Object & Manage Contents

#### Create Deque Object

Class `java.util.ArrayDeque` is an implementation of the `Deque` interface
* Instance of `ArrayDeque` can be created using:
	- No-arg constructor, creating a set of initial cacity of 16 elements
	- Constructor with specific initial capacity
	- Any other `Collection` (for example, `List`) to populate this deque with initial value
* ArrayDeque will auto-expand as more elements are added to it.

```java
	List<Product> list = new ArrayList<>();
	Deque<Product> products = new ArrayDeque<>();
	Deque<Product> products1 = new ArrayDeque<>(20);
	Deque<Product> products2 = new ArrayDeque<>(list);
```

#### Manage Deque Contents

`Deque` represents collection that implement **FIFO, LIFO** behaviors.
* `boolean offerFirst(T)` and `boolean offerLast(T)` insert elements at the head and the tail of the deque.
* `T pollFirst()` and `T pollLast()` get and remove elements at the head and the tail of the deque
* `T peekFirst()` and `T peekLast()` get elements at the head and the tail of the deque
* Null values are not allowed in a deque
* If deque is empty, poll and peek operations return null.

*Note:* `T getLast()` is an equivalent of the `T peekLast()`, but it oriduces an exception instead of returning null when the deque is empty.


### Create HashMap Object & Manage contents

#### Create HashMap Object

Class `java.util.HashMap` is an implementation of the `Map` interface.
`Map` is a composition of a `Set` of keys and a `List` of values.
* Instances of `HashMap` can be created using:
	- No-arg constructor, creating a set of initial capacity of 16 elements
	- Constructor with specific initial capacity
	- Constructor with specific initial capcity and load factor (default is 0.75)
	- Any other `Map` to populate this set with initial values
* HashMap will auto-expand its internal storage, when more elements are added to it.
* A read-only instance of `Map` can be created using:
	- Mehtod `of(<key>, <value>, ...)` overloaded for up to ten map entries
	- A var-arg method `ofEntries(Map.entry<key, value>... entries)`.
	
```java
	Map<Product, Integer> items = new HashMap<>();
	Map<Product, Integer> items1 = new HashMap<>(20);
	Map<Product, Integer> items2 = new HashMap<>(items);
	Map<Product, Integer> items3 = Map.of(new Product("Cake"), Integer.valueOf(2));
```

#### Manage HashMap Contents

`Map` represents collection of values with unique keys.
* `V put(K, V)` insert or update (when using an existing key) a key-value pair.
* `V remove(K)` delete a key-value pair
* `V get(K)` return the value for a given key.
* `boolean containsKey(K)` check existence of a key.
* `boolean containsValue(V)` check existence of a value.

*Notes:*
* `replace(<Key>)` or `replace(<key>, <value>)` operations as an alternative to `put` operation to update existing entries.
* Null key (just one) and null values are allowed in the HashMap.
Duplicate keys cannot be added to the map (uniquenes verified by the Set of keys):
	- `put` returns old value when updating the map entry for existing key or `null` if inserting new value.
	- `put` verifies if the element exists in the set using `equals` method.
	- `remove` returns the old value or `null` if map entry with this key is not found.
	- `get` returns the value for a key or `null` if map entry with this key is not found.
	
### Iterate through Collections

Collections implement `Iterable` interface, allowing them to be used in forEach loop.
* Any `List`, `Set`, or `Deque` can be directly used within a *forEach loop.*
* A more manual approach is to get an `Iterator` object from collection to step through the content.
* Get **Set of keys or list of values** from the `HashMap` to iterate through the content.
* Iterators also allow to *remove* content from the collection.

### Other Collection Behaviors

Some other examples of generic collection behaviors include:
* Convert collection to an array using `toArray` method
	- Use array provided if collection fits into it
	- Otherwise create new array with the matching size
* Remove elements from collection based on a condition
	- Implement interface `Predicate<T>` overriding abstract method `boolan test(T)`;
	- Use `removeIf(Predicate<T> p)` method to remove all matching elements from the collection
	
### Use `java.util.Collection` Class

Class `java.util.Collections` provides convenience methods for handling collections.
* **Filling collection with values**
* **Searching through the collection** of `Comparable` objects or using `Comparator`
* Reordering collection content using:
	- `Comparable` (as implemented by objects within the array)
	- `Comparator` interface.
	- `reverse` method changes the order to opposite
	- `shuffle` method randomly reorders collection.
	
### Access Collections Concurrently

Collection can be corrupted if accessed concurrently from multiple threads.
* If two or more concurrent execution paths (**threads**) within your program try to access the collection at the same time, they can corrupt it, if this collection is not immutable.
* Any object in a heap is not thread-safe if it is not immutable. Any thread can be interrupted, even when it is modifying an object, making other threads observe incomplete modification state.
* Making collection thread-safe does not guarantee the thread safety of the objects it contains. Only immutable objects are automatically thread-safe.

*Notes:*
Making collection thread-safe is not the same as making objects inside this collection thread-safe as well:
* Collection itself can be modifiable yet contain immutable elements. Working with such elements is thread-safe, but adding and removing them from the collection is not.
* Collection itself can be unmodifiable yet contain mutable elements. Adding or removing elements in the collection is thread-safe, but working with the objects in it is not.

### Prevent Collections Corruption

To prevent memory corruption in concurrently accessed collections, you can make collection:
* Unmodifiable (fast, but read-only)
* Synchronised (slow and unscalable)
* Copy-on-write (fast, but consumes memory)

[Concurrently explanation](/ocp-exam-11/-/blob/master/src/main/java/org/enable/ocp/exam/Capturescours/Collections1.png)

[Concurrently explanation end](/Capturescours/Collections2.png)

### Legacy Collection Classes

You may still encounter earlier versions of Java Collection API classes.
* Legacy collections were all defined as synchronized, which is a performance issue.
* With new collections (covered in this lesson), you can choose the type of thread-safe implementation.
* Other behaviors of old collections are almost identical to the new classes
* Examples of equivalent collection classes:
	- Class `ArrayList` is a new equivalent of the legacy class `Vector`
	- Class `HashMap` is a new equivalent of the legacy class `Hashtable`.

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam.streams;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author yvon
 */
public class TestTerminalOperation {

    static String[] values = Tests.values;

    public static void main(String[] args) {
        long v1 = Arrays.stream(values).filter(s -> s.indexOf("R") != -1).count();
        System.out.println(v1);
        int v2 = Arrays.stream(values).mapToInt(v -> v.length()).sum();
        System.out.println(v2);
        OptionalDouble v3 = Arrays.stream(values).mapToInt(v -> v.length()).average();
        System.out.println(v3.isPresent() ? v3.getAsDouble() : 0);
        Optional<String> v4 = Arrays.stream(values).max((s1, s2) -> s1.compareTo(s2));
        System.out.println(v4.isPresent() ? v4.get() : "");
        Optional<String> v5 = Arrays.stream(values).min((s1, s2) -> s1.compareTo(s2));
        System.out.println(v5.isPresent() ? v5.get() : "");

        //test reduce 
        int x = IntStream.of(5, 6, 9, 8, 2, 10).reduce(0, Integer::sum);
        System.out.println(x);
        var j=0;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam.streams;

import java.util.List;
import org.enable.ocp.exam.mocks.*;

/**
 *
 * @author yvon
 */
public class TestProduct {
    
    private static final List<Produit> list = MockBuilder.getInstance().mockData();
    
    public static void main(String[] args){
        StreamGroupingPartitioning.grouping(list);
        StreamGroupingPartitioning.partitioning(list);
        
        //parallel stream
        ParallelStreamTest.parallel(list);
        
    }
    
}

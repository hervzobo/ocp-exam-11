/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author yvon
 */
public class Item {
    private int id;
    private String name;
    
    public Item(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public static void main(String[] args){
        List<Item> list = Arrays.asList(
                new Item(1, "Screw"),
                new Item(2, "Nail"),
                new Item(3, "Bolt")
        );
        Stream<String> names = list.stream().sorted(Comparator.comparing(a -> a.getName())).map(a -> a.getName());
        names.forEach((t) -> { System.out.println(t);
                });
        list.stream().map(a -> a.getName()).sorted().forEach(System.out::println);
    }
}

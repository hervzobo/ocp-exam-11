/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author yvon
 */
public class Tests {
   
    static String[] values = {"RED", "GREEN", "BLUE"};
    public static void main(String[] args){
        boolean allGreen = Arrays.stream(values).allMatch(s -> s.equals("GREEN"));
        System.out.println(allGreen);
        boolean anyGreen = Arrays.stream(values).anyMatch(s -> s.equals("GREEN"));
        System.out.println(anyGreen);
        boolean noneGreen = Arrays.stream(values).noneMatch(s -> s.equals("GREEN"));
        System.out.println(noneGreen);
        Optional<String> anyColor = Arrays.stream(values).findAny();
        System.out.println(anyColor.get());
        Optional<String> firstColor = Arrays.stream(values).findFirst();
        System.out.println(firstColor.get());
        
        
        List<String> listOfWords = Arrays.asList("Cha", "ah", "oh", "ih");
        var list = new ArrayList<>();
        listOfWords.parallelStream().forEach(s -> {
            if(s.length() == 2){
                synchronized(list){
                    list.add(s);
                }
            }
        });
        list.forEach(s -> System.out.println(s));
        
        
        List<String> vals = Arrays.asList("a", "b", "c", "d", "e", "f", "g",
                "a", "b", "c", "d", "e", "f", "g",
                "a", "b", "c", "d", "e", "f", "g",
                "a", "b", "c", "d", "e", "f", "g");
        String join;
        join = vals.parallelStream().
                peek(System.out::println) //this shows how the elements are retrieved from the stream
                .reduce("_",
                        (a, b)->{ System.out.println("combining "+a+" and "+b+" Thread: "+Thread.currentThread().getName());
                        return a.concat(b);}
                ); 
        System.out.println(join);
        
        var val = Arrays.asList("a", "b");
        String string = val.parallelStream() 
                .reduce("_", (a, b)->a.concat(b)); 
        System.out.println(string);
    }
    
}

# Objectives
After completing this lesson, you should be albe to:
* Describe primitive types
* Describe operators
* Explain primitives type casting
* Use Math class
* Implement flow control with if/else and switch statements.
* Describe JShell

## Java Primitives
Java language provides eight primitive types to represent simple numeric, character, and boolean values.
- byte (8bits) [-128 ; 127]
- short (16bits) [-32768; 32767]
- int (32bits) [-2 147 483 648 ; 2 147 483 647]
- long (64bits) [-9 223 372 036 854 780 000 ; 9 223 372 036 854 780 000]
- float (32bits) [1.4E-45 ; 3.4028235E+38]
- double (64bits) [4.9E-324 ; 1.7976931348623157E+308]
- boolean, default value false (true or false)
- char (16bits), default value '\u0000'

## Declare and Initialize Primitive Variables
Primitive declaration and initialization rules:
* **Variable declaration and initialization syntax: ``<type> <variable name> = <value>;``**
* **A variable can be declared with no immediate, octal, decimal, and hex.**
* **Flot and double values can be expressed in normal or exponential notations.**
* **Multiple variables of the same type can be declared and initialized simultaneously.**
* **Assignment of one variable to another creates a copy of a value.**
* **Smaller types are automatically promoted to bigger types.**
* **Character values must be enclosed in single quotation marks.**

## Restrictions on Primitive Declarations and initializations
Primitive declaration and initialization restrictions:
* Variables must be initialized before use.
* A bigger type value cannot be assigned to a smaller type variable.
* Character values must not be enclosed in double quotation marks.
* A character value cannot contain more than one characte.
* Boolean values can be expressed only as true or fale.

## Java Operators
List of Java operators in the order of precedence
* postfix increment and decrement : ``++, --``
* prefix increment and decrement, and unary ``++, --, +, -, ~, !``
* multiplicative: ``* / %``
* additive ``+, -``
* bit shift: ``<< >> >>>``
* relational: ``< >, <= >= , instanceof``
* equality: ``== ; !=``
* bitwise AND : ``&``
* bitwise exclusive OR: ``^``
* bitwise inclusive OR: ``|``
* logical AND : ``&&``
* logical OR: ``||``
* ternary : ``? :``
* assignment: ``=, +=, -=, /=, %=, &=, ^=, |=, <<=, >>=, >>>=`` 

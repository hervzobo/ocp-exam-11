/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam;

import java.util.stream.Stream;

/**
 *
 * @author yvon
 */
public class Main {

    public static void main(String[] args) {
        Stream.of("A", "C", "B", "D", "B", "D")
                .distinct()
                .sorted()
                .skip(2)
                .forEach(s -> System.out.println(s));

        System.out.println("Hello world!");
        Stream.of("B", "C", "A", "E", "D", "F")
                .takeWhile(s -> !s.equals("D"))
                .dropWhile(s -> !s.equals("C"))
                //.limit(2)
                .forEach(s -> System.out.println(s));
        System.out.println("Hello world!");
        Stream.of("B", "C", "A", "E", "D", "F")
                .dropWhile(s -> !s.equals("C"))
                .limit(2)
                .forEach(s -> System.out.println(s));
    }
}

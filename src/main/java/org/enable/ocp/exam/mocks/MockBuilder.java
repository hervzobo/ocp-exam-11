package org.enable.ocp.exam.mocks;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class MockBuilder {
    private static MockBuilder instance;

    private MockBuilder(){
        
    }
    public static synchronized MockBuilder getInstance(){
        if (instance == null){
            instance = new MockBuilder();
        }
        return instance;
    }

    public List<Produit> mockData(){
        List<Produit> produits = new ArrayList<>();
        LocalDate toDay = LocalDate.now();

        for (int index=0; index<50; index++){
            toDay = toDay.minusMonths(1).minusDays(2);
            Produit produit = new ProduitBuilder().setDesignation("Luciole"+index)
                    .setPrixCash(150000)
                    .setPrixCredit(200000)
                    .setDateMiseEnVente(toDay)
                    .build();
            if (index%2 > 0){
                produit.setPrixCash(produit.getPrixCash()+10000);
                produit.setDateMiseEnVente(LocalDate.now());
                produit.setDesignation("Lion");
                produits.add(new Drink(produit.getDesignation(), produit.getPrixCash(), produit.getPrixCredit(), toDay));
            }
            produits.add(produit);
        }
        return produits;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam.mocks;

import java.util.List;

/**
 *
 * @author yvon
 */
public class ParallelStreamTest {
    
    public static void parallel(List<Produit> list){
        double v = list.parallelStream().mapToDouble(p -> p.getPrixCash().doubleValue()).sum();
        System.out.println("chiffre d'affaire en cash: "+v);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam.mocks;

import java.time.LocalDate;

/**
 *
 * @author yvon
 */
public class Drink extends Produit{

    public Drink(String designation, Integer prixCash, Integer prixCredit, LocalDate dateMiseEnVente) {
        super(designation, prixCash, prixCredit, dateMiseEnVente);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.enable.ocp.exam.mocks;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author yvon
 */
public class StreamGroupingPartitioning {
    /**
     * divides content into a map of multiple key values using Function
     * @param list 
     */
    public static void grouping(List<Produit> list){
        Map<LocalDate, List<Produit>> productGroups = list.stream()
                .collect(Collectors.groupingBy(p -> p.getDateMiseEnVente()));
        System.out.println("Grouping size: "+productGroups.size());
        System.out.println("products with key: 2021-01-15: "+productGroups.get(LocalDate.parse("2021-01-15")));
    }
    /**
     * divides content into a map with two key values (boolean true/false) using predicate
     * @param list 
     */
    public static void partitioning(List<Produit> list ){
        Map<Boolean, List<Produit>> productTypes = list.stream()
                .collect(Collectors.partitioningBy(p -> p instanceof Drink));
        System.out.println("Partioning size: "+productTypes.size());
        System.out.println("Drink products: "+productTypes.get(true));
    }
    
}

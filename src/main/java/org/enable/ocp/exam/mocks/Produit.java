package org.enable.ocp.exam.mocks;



import java.time.LocalDate;



public class Produit {
    private String designation;
    private Integer prixCash;
    private Integer prixCredit;
    private LocalDate dateMiseEnVente;

    public Produit(String designation, Integer prixCash, Integer prixCredit, LocalDate dateMiseEnVente) {
        this.designation = designation;
        this.prixCash = prixCash;
        this.prixCredit = prixCredit;
        this.dateMiseEnVente = dateMiseEnVente;
    }

    
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getPrixCash() {
        return prixCash;
    }

    public void setPrixCash(Integer prixCash) {
        this.prixCash = prixCash;
    }

    public Integer getPrixCredit() {
        return prixCredit;
    }

    public void setPrixCredit(Integer prixCredit) {
        this.prixCredit = prixCredit;
    }

    public LocalDate getDateMiseEnVente() {
        return dateMiseEnVente;
    }

    public void setDateMiseEnVente(LocalDate dateMiseEnVente) {
        this.dateMiseEnVente = dateMiseEnVente;
    }

    @Override
    public String toString() {
        return "Produit{" + "designation=" + designation + ", prixCash=" + prixCash + ", prixCredit=" + prixCredit + ", dateMiseEnVente=" + dateMiseEnVente + '}';
    }

    
}

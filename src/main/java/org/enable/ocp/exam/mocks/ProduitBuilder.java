package org.enable.ocp.exam.mocks;


import java.time.LocalDate;

public class ProduitBuilder {
    private String designation;
    private Integer prixCash;
    private Integer prixCredit;
    private LocalDate dateMiseEnVente;

    public ProduitBuilder setDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public ProduitBuilder setPrixCash(Integer prixCash) {
        this.prixCash = prixCash;
        return this;
    }

    public ProduitBuilder setPrixCredit(Integer prixCredit) {
        this.prixCredit = prixCredit;
        return this;
    }

    public ProduitBuilder setDateMiseEnVente(LocalDate dateMiseEnVente) {
        this.dateMiseEnVente = dateMiseEnVente;
        return this;
    }

    public Produit build(){
        return new Produit(designation,prixCash,prixCredit,dateMiseEnVente);
    }
}

# Handle Exceptions and Fix Bugs

## Resource Auto-Closure

Try-with-parameters syntax provides auto-closure of resource
* Classes that implement `AutoCloseable` interface can be instantiated using *try-with-parameters* syntax
* Mutiple resources may be initialized inside try-with-parameters construct
* Automatic closure of such resources is provided by an implicitly formed finally block

```java
	try(/*initialize autocloseable ressource*/){
		/* use resources */
	} catch(Exception e){
		/*handle exception*/
	}
	
	/*finally block invoking close method on every autocloseable resource is formed implicitly */
```

## Suppressed Exceptions

Auto-closure of a resource may produce suppressed exceptions.
Consider the following example:
* One *exception is produced in the try block*
* Another *exception is produced in the implicitly formed finally block* (thrown by the close method).
* Method `getSuppressed` returns a list of suppressed exceptions.

## Validate Program Logic Using Assertions

Assertions can be used to verify the application is executing as expected.
* Assertions test for failure of various conditions. 
`assert <boolean expression>`;
`assert <boolean expression>: <error text expression>`
* When assertion expression is `false`, application terminates and assertion error information is printed.

```java
	Set<String> values = new HashSet();
	String value = "acme";
	boolean existingValue = values.add(value);
	assert existingValue: "Value"+value+" already exists in the set"
```

* Assertions are disabled by default:
	- Never assume they'll be executed (not used in production code)
	- Do not use assertion to validate parameters or user input
	- Do not create assertions that cause any state changes or other side effects in the program flow.
* To enable assertions, use `-ea` or `-enableassertions` commande-line option.
`java -ea <package>.<MainClass>`

*Note:* Assertions are designed for code testing purposes. If you need to validate parameters or user input, it is better to achieve this by other means, such as use BeanValidation API or throw exceptions such as an IllegalArgumentException.

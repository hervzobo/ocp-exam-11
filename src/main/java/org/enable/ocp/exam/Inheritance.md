# Extend Classes

Classes form a hierarchy descending from the `java.lang.Object` class.
* Class Object is an ultimate parent of any other class in Java.
* Class Object defines common, generic operations that all other Java classes inherit and reuse.
* There is no pratical difference between:
```java
public class Product {}
```

and 
```java
public class Product extends Object {}
```

because the `extends Object` clause is implied when an explicit extends clause is not present

* The explicit extends describes which specific class should be extended instead of the Object class.
`public class Food extends Product {}`

* Parent class (the one you extend) is known as the superclass.
* Child class (the one that extends the parent) is known as the subclass.
* A class can have one immediate parent, as mutiple inheritance is not allowed in Java.

*Note:* In the example, `Food` is a child of `Product` and a grandchild of `object`.

## Object Class
Object class contains generic behaviors that are inherited by all Java classes, such as:
* The `toString` method creates text value for the object.
* The `equals` method compares a pair of objects.
* The `clone` method produces a replica of the object.
* The `hashCode` method generates int hash value for the object.
* `wait`, `notify`, and `notifyAll` methods control threads.

Operations inherited frm the Object class
* `protected Object clone()`: creates and returns a copy of this object
* `public boolean equals(Object obj)`: oindicates whether some other object is "equal to" this one
* `protected void finalize()`: Deprecated, because the finalization mechanisme is inherently problematic
* `publi final class<?> getClass()`: Returns the runtime class of this object
* `public int hashCode()`: Returns a hash code value for the object
* `public final void notify()`: Wakes up a single thread that is waiting on this object's monitor
* `public final void notifyAll()`: Wakes up all threads that are waiting on this object's monitor
* `public String toString()`: Returns a string representation of the object.
* `public final void wait()`: Causes the current thread to wait until it is awakened, typically by being notified or interrupted
* `public final void wait(long timeoutMillis)`: Causes the current thread to wait until is awakened, tipycally by being notified or interrupted or until a certain amount of real time has elapsed
* `public final void wait(long timeoutMillis, nt nanos)`: Causes the current thread to wait until it is awakened, typically by being notified or interrupted or until a certain amount of real time has elapsed.

## Reuse Parent Class Code Through Inheritance

The purpose of inheritance is to reuse generic superclass behaviors and state in subclasses.
* **Superclass** represents a more generic, parent type
* Superclasses define common attributes and behaviors.
* **Subclass** represents a more specific, child type that *extends the parent type*.
* Subclasses inherit all attributes and behaviors from their parents.
* Subclasses may define subtype-specific attributes and behaviors.

## Instantiating Classes and Accessing Objects

Heap memory allocated to store object (class instance) that contains:
* Code of the specific subtype
* All of the parents up the class hierarchy

Object reference can be of generic or specific type, projecting the entire object or only some of its methods and variables.

## Rules of Reference Type Casting

An object can be referenced using either of the following:
- Specific child subclass type
- Generic parent superclass types

**To invoke an operation on the object, reference type has to be specific enough to be at leat at the level in the class hierarchy where the operation was first declared.**

Type casting rules:
* *Casting is required to assign parent to child reference type*
* *No casting is required to assign child to parent reference type*
* *Casting is not possible between objects of sibling types.*

## Verify Object Type Before Casting the Reference

Methods should normally be defined using generic (superclass) parameter and return value types.
* You need not verify or cast the reference type to invoke *generic operations.*
* You should verify the object type by using the *instanceof* operator before **casting reference to a specific type.**
* Invoke **subtype specific operations** using a specific reference type.

*Note:* if object is null, the instanceof operator returns a false value.

## Reference Code within the Current or Parent Object

Reference the current object and the parent object.

* Use the `this` keyword to reference variables and methods of the current object.
* Use the `super` keyword to reference varialbes and methods of the parent object
* The `this` or `super` kyword is not required **when the reference is not ambiguous**

Reminders:
* Access modifiers (default or private) would prevent subclass from accessing parent class variables and methods.
* Well-encapsulated code should expose methods and should hide variables.


## Define Subclass Constructors

The subclass constructor must invoke the superclass constructor.
* Superclass contains the no-arg constructor (default or explicitly defined).
* Subclass can implicitly invoke the superclass constructor.
* Superclass does not provide the no-arg constructor (only constructors with parameters are present).
* Subclass constructor must explicitly invoke superclass constrctor.
* Superclass constructor is invoke by using the `super` keyword with **matching constructor signature.**
* Invocation of superclass constructor must be the first line of code in the subclas constructor.

**Reminder:** Parameter names are irrelevant; you must match types and number of parameters when invoking the superclass constructor.

## Override Methods and Use Polymorphism

The subclass can override parent class methods.
* The subclass defines the methosd whose signature matches parent class method.
* The subclass can wident, but cannot narrow access of methods it overrides.
* Polymorphism (many forms) in Java means when a methods is declared in a sperclass and is overriden in a sublcass, the sublcass method takes precedence without casting reference to a specific subclass type. 

*Note:* Annotation **@Override** is optional; it is used to ensure that sublcass method siganture matches the superclass method.

## Define Abstract Classes and Methods

The `abstract` keyword can be used to encourage class extensibility.
* Class cannot be directly instantiated if it is marked with the **abstract** keyword
* The abstract class purpose is to be extended by one or more concrete subclasses.
* The abstract class can contain normal variables and methods, which are inherited by its subclasses as usual.
* It may also contain abstract methods that describe method signatures, wihout a method body.
* Concrete subclasses must override all abstract methods of their abstract parent.

## Define Final Classes and Methods

The `final` keyword can be used to limit class extensibility.
* Class cannot extend a class that is marked with the **final** keyword.
* The subclass cannot verride a superclass method that is marked with the **final** keyword.

*Notes:* 
* Attempt to override a final method or extend a final class would result in compilation error.
* A variable becomes a constant (cannot be reassigned) if it is marked with the final keyword.

## Overrid e Object Class Operations: toString

It is recommended that all Java classes override some operations defined by the Object class.
* The `toString` method produces test value for the object, which can be easily used in logging.
* Various operations in JDK classes use the `toString` operation.
* A specific subclass version of the operation automatically takes precedence due to Java polymorphism.
* The subclass version of the method may choose to *reuse* or rredefine superclas operation logic.

## Override Object Class Operation: equals

It is reommended that all Java classes override some operations defined by Object class.
* The `equals` method compares objects
* This is used to identify if objects should be considered identical.

*Notes:*
- The `==` operator compares values in the stack. It can be used to compare primitive values or to determine if two references are poiting to the same object.
- The overriding method `equals` enables you to compare object content in th heap.
- Java classes such as `String, Number, LocalDate,` and so on override the `equals`method.

## Override Object Class Operations: hashCode

It is recommanded that all Java classes override some operations defined by the Object class.
* The `hashcode` method generates object identify as an `int` value. Must consistently return the same `int` value for the same instance. Usfor bucketing hashed collections, such as HashSet, HashMap, Hashtable.
* The `hashCode` method should return the same int value for any pair of objects that are considered to be the same when compared with the equals method.
* The `Objects` class contains the *hash* method that generates a hash code value for a number of objects.


## Compare String Objects

Class String represents an unusual case:
* String objects are interned; therefore, multiple references can point to the same String object.
* This makes the `==` operator work with Strings in a way that is similar to the primitives.
* To avoid confusion, compare Strings like any other objects by using the `equals` method.
* In addition to the `equals` method, the String class also provides an `equalsIgnoreCase` method.
* The String class is final and cannot be extended.

*Reminder*:
- Instances of String can and should be created without explicitly invoking a String constructor.
- String is the only Java object that allows simplified instantiation as just a text value enclosed within double quotes: "some text" and that is a recommended approach.
- Strings are interned objects; a single copy of a String literal is stored Pool memory area.

## Factory Method Pattern

Factory methods can hide the use of a specific constructor.
* Dynamically choose the subtype instance to be created.
* Analyze conditions and produce an instance of a specific subtype.
* Invokers can remain subtype unaware.
* Later addition of extra subtypes may not affect such invokers.

```java
public class ProductFactory {
 	public static Product createProduct(...){
 		switch(productType) {
 			case FOOD:
 				return new Food(...);
 			case DRINK:
 				return new Drink(...);
 		}
 	}
 }
``` 
 
```java
public abstract class Product {
  	public Product(...) {}
  }
```
  
```java
  public class Food extends Product {
	public Food(...){}
   }
```
   
```java 
public class Drink extends Product {
   		public Drink(...) {}
   	}
```

The actual decision can be based on factory method parameters or other dynamically determined factors,
```java 
Product product = ProductFactory.createProduct(...);
```


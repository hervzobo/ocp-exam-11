# Characteistics of Streams

Stream is an immutable flow of elements.
* Stream processing can be *sequential* (default) or *parallel*.
* Once an element is processed, it is no longer available from the stream.
* Stream pipeline traversal uses *method chaining* intermediate operations return streams.
* Pipeline traversal is lazy:
	- Intermediate actions are deferred until stream is traversed by the terminal operation.
	- The chain of activities could be fused into a single pass on data.
	- Stream processing ends ans soon as the result is determined; remaining stream data can be ignored.
* Stream operations use functional interfaces and can be implemented as *lambda expressions*.
* Stream may represent both finite and infinit flows of elements.

Remarque: Processing of streams lazily allows for significant efficiencies; for example, if a pipeline needs to perform a chain of activities such as filtering, mapping, and summing, it can fused into a single pass on the data, with minimal intermediate state. Laziness also allows avoiding examining all the data when it is not necessary; for operations such as "find the first string longer than 1000 characters", it is only necessary to examine just enough strings to find one that has the desired characteristics without examining all of the strings available from the source. (This behavior becomes even more important when the input stream is infinite and not merely large.)

## Create Streams Using Stream API

Streams handling is described by the following interfaces:
* `BaseStream` - defines core stream behaviors, such as mapping the stream in a parallel or sequential mode.
* `Stream`, `DoubleStream`, `IntStream`, `LongStream` interfaces extend the `BaseStream` and provide stream processing operations.
* Streams use generics.
* To avoid excessive boxing and unboxing, primitive stream variants are also provided.
* Stream can be obtained from any **collection and array** or by using **static methods** of the `Stream` class.
* Many other Java APIs can create and use streams.


## Stream Pipeline Processing Operations

Stream handling operation categories:
* Intermediate - perform action and produce another stream: **filter, map, flatMap, peek, distinct, sorted, dropWhile, skip, limit, takeWile**.
* Terminal - traverse stream pipeline and end the stream processing: **forEach, forEachOrdered, count, min, max, sum, average, collect, reduce, allMatch, anyMatch, noneMatch, findAny, findFirst**.
* Use *functional interface* from `java.util.function` package.
* Can be implemented using lambda expressions
* Basic function purposes:
	- `Predicate` performs tests
	- `Function` converts types
	- `UnaryOperator` (a variant of function) converts values
	- `Consumer` processes elements
	- `Supplier` produces elements
	
``` Stream.generate(<Supplier>)
	.filter(<Predicate>)
	.peek(<Consumer>)
	.map(<Function>/<UnaryOperator>)
	.forEach(<Consumer>);	
```

*Note:* Operation `sum` and `average` are available only for primitive stream variants DoubleStream, IntStream, LongStream.

## Using Functional Interface

Stream operations use functional interfaces:
* Located in `java.util.function` package
* Can be implemented using lambda expressions
* Basic function shapes are:
	- `Predicate<T>` defines method boolean tes(T t) to apply conditions to filter elements
	- `Function<T, R>` defines method R apply(T t) to convert types of elements
	- `UnaryOperator<T>` (variant of Function) defines method T apply(T t) to convert values
	- `Consumer<T>` defines method void accept(T t) to process elements
	- `Supplier<T>` defines method T get() to produce elements.

```java List<Product> list = new ArrayList();
 list.stream() // Produces stream of products from the list
 	.filter(p -> p.getDiscount() == 0) // Retain only products with no discount
 	.peek(p -> p.applyDiscount(0.1)) // Apply discount of 10 percent
 	.map(p -> p.getBestBefore()) // produce a stream of LocalDate objects
 	.forEach(p -> p.plusDays(1)); //calculate the next day
```

## Bi-argement Variants of Functional Interfaces

Process more than one value at a time.
* Extra parameter is provided compared to basic function interfaces:
	- `BiPredicate<T, U>` defines method boolean test(T t, U u) to apply conditions
	- `BiFunction<T, U, R>` defines method R apply(T t, U u) to convert two types into a single result.
	- `BinaryOperator<T>` (variant of BiFunction) defines method T apply(T t1, T t2) to convert two values
	- `BiConsumer<T>` defines method void accept(T t, U u) to ptocess a pair of elements
	
### Perform Actions with Stream Pipeline Elements

Intermediate or teminal actions are handled by `peek` and `forEach` operations.
* Operations `peek`, `forEach` and `ForEachOrdered` accept `Consumer<T>` interface.
* Lambda espression must implement abstract `void accept(T t)` method.
* Default `andThen` method provided by `Consumer` interface combines consumers together.
* Difference between `forEachOrdered` and `ForEach` is that the `forEach` operation does not guarantee respecting the order of elements, which is actually beneficial for parallel stream processing.

### Perform Filtering of Stream Pipeline Elements

Filtering of the pipeline content is performed by the `filter` operation.
* Method `filter` accepts `Predicate<T>` interface and returns a stream comprising only elements that statisfy the filter criteria.
* Lambda expression must implement abstract `boolean test(T t)` method.
* Default methods provided by the `Predicate`:
	- `and` combines predicates like the `&&` operator.
	- `or` combines predicates like the `||` operator.
	- `negate` returns a predicate that represents the logicial negation of this predicate.
* Static methods provided by the `Predicate` interface:
	- ```java not``` returns a predicate that is the negation of the supplied predicate.
	- `isEqual` returns a predicate that comares the supplied object with the contents of the collection.
```java 
Predicate<Product>` foodFilter = p -> p instanceof Food;

Predicate<Product> priceFilter = p -> p.getPrice().comporeTo(BigDecimal.valueOf(2)) < 0;
list.stream().filter(foodFilter.negate().or(priceFilter))
	.forEach(p -> p.setDiscount(BigDecimal.valueOf(0.1)));
```

### Perform Mapping of Stream Pipeline Elements

Map stream elements to a new stream of different content type using `map` operation.
* Method `map` accepts a `Function<T, R>` interface and returns a new stream comprising elements produced by this function based on the original stream content.
* Lambda expression must implement abstract `R apply(T t)` method.
* Default methods provided by the `Function`:
	- `andThen` and `compose` combine functions together.
* Static methods provided by the `Function` interface:
	- `identify` returns a function that always returns its input arguement (equivalebt of t `t -> t` function).
* Primitive variants of `map` are: `mapToInt, mapToLong, mapToDouble`.
* Interface `UnaryOperator<T>` is a variant of a `Function` that maps values without changing the type.

```java
Function<Product, String> nameMapper = p -> p.getName();
UnaryOperator<String> trimMapper = n ->n.trim();
ToIntFunction<String> lengthMapper = n -> n.length();
list.stream().map(nameMapper.andThen(trimMapper)).mapToInt(lengthMapper).sum();
```

*Note:*
* Difference between `andThen` and `compose` is the order in which functions are combined.
* In the example above, `nameMapper` is applied before `trimMapper`.

The example could have been written as a single Function defined as an inline lambda expression:
```java
list.stream().mapToInt(p -> p.getName().trim().length()).sum();
```
The reason behind having multiple maps is to be able to use other intermediate operations, such as filter or peek between these mappings.


### Join Streams using `flatMap` Operation

Flatten a number of streams into a single stream:
* Operation `stream<R> flatMap(Function<T, Stream<R>> f)` merge streams.
* Primitive variants are: `flatMapToInt, flatMapToLong, flatMapToDouble`

```java
public class Order {
	private List<Product> items;
	public Streqm<Product> items() {
		return items.stream();
	}
}

List<Order> orders = new ArrayList();
double x = orders.stream()
		.flatMap(order -> order.items())
		.filter(item -> item.getName().equals("Tea"))
		.mapToDouble(item -> item.getPrice().doubleValue())
		.sum();
```

The example processes a stream of Order objects, each containing a stream of product objects, flattened into a single stream of products. All products except Tea are filtered out, and a total sum of all Tea products sold in any of the orders is calculated.

### Other Intermediate Stream Operations

Intermediate operations rearranging or reducing stream content:
* Operation `distinct()` returns a stream with no duplicates.
* Operations `sorted(), sorted(Comparator<T> t)` rarrange the order of elements
* Operation `skip(long l)` skips a number of elements in the stream
* Operation `takeWhile(Predicate p)` take elements from the stream while they match the predicate.
* Operation `dropWhile(Predicate p)` removes elements from the stream while they match the predicate.
* Operation `limit(long l)` returns a stream of elements limited to given size.

```java
Stream.of("A", "C","B", "D", "B", "D")//ACBDBD
	.distinct()//ACBD
	.sorted()//ABCD
	.skip(2)//Ignore AB
	.forEach(s -> s.toLowerCase());//cd
	
	
	Stream.of("B", "C", "A", "E", "D", "F")//BCAEDF
		.takeWhile(s -> !s.equals("D"))//BCAEF
		.dropWhile(s -> !s.equals("C"))//CAE
		.limit(2)//CA
		.forEach(s  -> s.toLowerCase());//CA
```

* Operations `takeWhile` and `dropWhile` may produce different results if the stream is ordered.
* Their cost can be significantly increased for ordered and parallel streams.

### Short-Circuit Terminal Operations

Short-circuit terminal operations produce finite result even if presented with infinite input.
* All short-circuit operations trminate stream pipeline as soon as result is omputed.
* Operation `allMatch(Predicate p)` returns true if all elements in the stream match the predicate.
* Operation `anyMatch(Predicate p)` returns true if any elements in the stream match the predicate.
* Operation `nonMatch(Pricate p)` returns thue if no  elements in the stream match the predicate.
* Operation `findAny()` returns an element from the stream wrapped in `Optional` object.
* Operation `findFirst()` returns the first element from the stream wrapped in `Optional` object.

```java
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author yvon
 */
public class Tests {
   
    static String[] values = {"RED", "GREEN", "BLUE"};
    public static void main(String[] args){
        boolean allGreen = Arrays.stream(values).allMatch(s -> s.equals("GREEN"));
        System.out.println(allGreen);
        boolean anyGreen = Arrays.stream(values).anyMatch(s -> s.equals("GREEN"));
        System.out.println(anyGreen);
        boolean noneGreen = Arrays.stream(values).noneMatch(s -> s.equals("GREEN"));
        System.out.println(noneGreen);
        Optional<String> anyColor = Arrays.stream(values).findAny();
        System.out.println(anyColor.get());
        Optional<String> firstColor = Arrays.stream(values).findFirst();
        System.out.println(firstColor.get());
    }
    
}
```

### Process Stream Using `count, min, max, sum, average` Operations

Terminal operations calculate values from stream content.
* Method `count` returns a number of elements in the stream;
* Method `sum` and `average` are available only for primitive stream variants (int, long, double).
* Method `average` returns an `OptionalDouble` object (primitive variant for `Optional` class).
* Methods `min` and `max` retruns an `Optional` object wrapper for the minimum or maximum value from the stream, according to the `Comparator` supplied.

```java 
import java.util.Arrays;
import java.util.Optional;
import java.util.OptionalDouble;

/**
 *
 * @author yvon
 */
public class TestTerminalOperation {

    static String[] values = Tests.values;

    public static void main(String[] args) {
        long v1 = Arrays.stream(values).filter(s -> s.indexOf("R") != -1).count();
        System.out.println(v1);
        int v2 = Arrays.stream(values).mapToInt(v -> v.length()).sum();
        System.out.println(v2);
        OptionalDouble v3 = Arrays.stream(values).mapToInt(v -> v.length()).average();
        System.out.println(v3.isPresent()?v3.getAsDouble():0);
        Optional<String> v4 = Arrays.stream(values).max((s1, s2) -> s1.compareTo(s2));
        System.out.println(v4.isPresent()?v4.get():"");
        Optional<String> v5 = Arrays.stream(values).min((s1, s2) -> s1.compareTo(s2));
        System.out.println(v5.isPresent()?v5.get():"");
    }

}
```

### Aggregate Stream Data using `reduce` Operation

Produce a single result from the stream of values using `reduce` operation:
* `Optional<T> reduce(BinaryOperator<T> accumulator)` performs accumulation of elements.
* `T reduce(T identity, BinaryOperator<T> accumulator)` identity acts as initial (default) value.
* `<U> reduce(U identity, BiFunction<U, T, U> accumulator, BinaryOperator<U> combiner)` BiFunction performs both value mapping and accumulation of values.
BinaryOperator combines results produced by the BiFunction in parallel stream handling mode.

```java 
	Optional<String> x1 = list.stream()
				.map(p -> p.getName())
				.reduce((s1, s2) -> s1+" "+s2);
				
	//test reduce 
	int x = IntStream.of(5, 6, 9, 8, 2, 10).reduce(0, 			Integer::sum);
	System.out.println(x);
```

## General Logic of the `collect` Operation

Perform a mutable reduction operation on the elements of the stream.
* Method `collect` accepts `Collector` interface implementation, which:
	- *Produces new result contains using `supplier`*
	- *Accumulates data elements into these result containers using * `BiConsumer`
	- *Combines result containers using * `BinaryOperator`
	- *Optionally performs a final transform of the processing result using the * `Function`
* Class `Collectors` presents a number of predefined implementations of the `collector` interface.

```
	stream.collect(<supplier>, <accumulator>, <combiner>)
	stream.collect(<collector>)
	stream.collect(Collectors.collectingAndThen(<collector>, <finisher>))
```

Operations `collect` and `reduce` both perform reduction. However, `collect` operation accumulates results within intermediate containers, which may improve performance.

* Method `collect` accepts either a `Collector` implementation (you may use existing implementations supplied by the `Collectors` class):
	- `<R, A> R collect(Collector<? super T, A, R> collector)`

Or supplier, accumultor, and combiner provided separately (nt wrapped into a Collector objet):
	- <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator BiConsumer<R, R> combiner).
	
### Using Basic Collectors

Predefined implementations of the `Collector` interface supplied by `collectors` class:
* **Calculating summary values** such as average, min, max, count, sum
* **Mapping** and **joining** stream elements
* **Gathering** stream elements into a collection such as list, set or map.

```java
	DoubleSummaryStatistics stats = list.stream()
			.collect(Collectors.summarizingDouble(p ->p.getPrice().doubleVqlue));
			
	String s = list.stream()
		.collect(Collectors.mapping(p -> p.getName(), Collectors.joining(",")));
		
	List<Product> drinks = list.stream()
		.filter(p -> p instanceof Drink)
		.collect(Collectors.toList());
	
```

Calculating and using summary statistics object:
```java
	 DoubleSummaryStatistics statistics = list.stream().collect(Collectors.summarizingDouble(s -> s.getPrice().doubleValue()));
	 
	long count = statistics.getCount();
	double avg = statistics.getAverage();
	double min = statistics.getMin();
	double max = statistics.getMax();
	double sum = statistics.getSum();
```

### Perform a Conversion of a Collector Result

Add finisher function to a collector to perform conversion of the collect result.
* Method `collectors.collectingAndThen` appends a finishing `Function` to a `collector`

```java
	NumberFormat fmt = NumberFormat.getCurrencyInstance(Locale.UK);
	String s2 = list.stream()
		.collect(Collectors.collectingAndThen(
		collectors.averagingDouble(p -> p.getPrice().doubleValue()), n -> fmt.format(n)));
```

### Perform Grouping or Partitioning of the Stream Content

Class Collectors proides collector objects to subdivide stream elements into partition or groups:
* *Partitioning* divides content into a map with two key values (boolean true/false) using **predicate.**
* *Grouping* divides content into a map of multiple key values using **Function.**

```java
	Map<Boolean, List<Product>> productTypes = 
		list.stream()
			.collect(Collectors.partitioningBy(p ->p instanceof Drink));
			
	Map<LocalDate, List<Product>> productGroups = 
		list.stream()
			.collect(collectors.groupingBy(p -> p.getBestBefore()));
```

### Mapping and Filtering with Respect to Groups or Partitions

Mapping and filtering in a multilevel reduction, using the downstream grouping or partitioning:
* `flatMapping` collector is applied to each input element in the stream before accumulation.
* `filtering` collector eliminates content from the stream without removing an entire group, if the group turns out to be empty.

```java
	Map<Customer, Set<Product>> customerProducts = 
		.orders.collect(Collectors.groupingBy(o ->o.getCustomer(),
		Collectors.flatMapping(o -> o.getItems().stream(), Collectors.toSet())));
		
	Map<Customer, Set<Order>> customerOrderesOnDate =
		.orders.collect(Collectors.groupingBy(o -> o.getCustomer(), Collectors.filtering(o -> o.getDate().equals(LocalDate.of(2020,11,25)), Collectors.toSet())));
```

## Parallel Stream Processing

Parallel stream processing logic:
* Elements of the stream are subdivided into subsets.
* Subsets are processed in parallel.
* Subsets may be subdivided into further subsets.
* Processing order is stochastic (indeterinate).
* Subsets are then combined.
* Turn parallelism on or off using the `paralel` or `sequential` (default) methods.
* Entire stream processing will turn sequential or parallel depending on which method was invoked last.

```java
	double v = list.stream().parallel().mapToDouble(p -> p.getPrixCash().doubleValue()).sum();
	
```

*Note:* Parallel stream processing can be triggered using `parallelStream` operation available for any collection:

```java
	 double v = list.parallelStream().mapToDouble(p -> p.getPrixCash().doubleValue()).sum();
	
```

### Parallel Stream Processing Guidelines

Parallel stream processing should observe the following guidelines:
* **stateless** (state of one element must not affect another element)
* **Non-interfering** (data source must not be affected)
* **Associative** (result must not be affected by the order of operands).

### Restrictions on Parallel Stream Processing

Incorrect handling of parallel streams can corrupt memory and slow and down processing.
* Do not perform operations that require sequential access to a shared resource.
* Do not perform operations that modify shared resource.
* Use appropriate Collectors, such as:
	- `toMap` in sequential mode
	- `toConcurrentMap` in parallel mod
Parallel processing of the stream can only be beneficial if:
* Stream contains large number of elements
* Multiple CPU cores are available to physically parallelize computations
* Processing of stream elements requires significant CPU resources.
 



